//
//  InterceptCalculator.h
//  Solaris
//
//  Created by Adrian Duyzer on 12-07-02.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  This class, which is based on the intercept_math.rb class, returns the intercept point
//  (point of intersection) that is created by a line drawn between two points that is extended
//  to touch the edge of the screen.

#import <Foundation/Foundation.h>

@interface InterceptCalculator : NSObject

+ (CGPoint)findInterceptFromSource:(CGPoint)source andTouch:(CGPoint)touch withinBounds:(CGRect)bounds;

@end