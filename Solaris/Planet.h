//
//  Planet.h
//  Solaris
//
//  Created by Adrian Duyzer on 12-07-16.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Planet : NSObject

- (id)initWithPosition:(CGPoint)position;

- (CGPoint)position;

@property (strong, nonatomic) CCSprite *sprite;
@property (nonatomic) CGRect rect;

@end
