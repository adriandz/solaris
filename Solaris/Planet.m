//
//  Planet.m
//  Solaris
//
//  Created by Adrian Duyzer on 12-07-16.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Planet.h"

@interface Planet()

@end

@implementation Planet

@synthesize rect = _rect;
@synthesize sprite = _sprite;

- (id)initWithPosition:(CGPoint)position
{
    if ((self = [super init])) {
        self.sprite = [CCSprite spriteWithFile: @"mars.png"];
        self.sprite.position = position;
        self.rect = CGRectMake(self.sprite.position.x - (self.sprite.contentSize.width/2), 
                               self.sprite.position.y - (self.sprite.contentSize.height/2), 
                               self.sprite.contentSize.width, 
                               self.sprite.contentSize.height);
    }
    return self;
}

- (CGPoint)position
{
    return self.sprite.position;
}

@end