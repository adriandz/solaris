//
//  Ship.h
//  Solaris
//
//  Created by Adrian Duyzer on 12-02-19.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//  This is the ship that flies around, the the player controls, etc.
//  How does it behave?  Well, it has an angle and a distance, and these
//  are set by the SolarisLayer as the player controls them, and so on.
//  Actually, it doesn't make sense to have them set by the SolarisLayer.
//  They really ought to be derived from the ship itself, although
//  we may allow them to be set initially when the ship is created.

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Ship : NSObject

@property (nonatomic) float angle;
@property (nonatomic) float distance;
@property (strong, nonatomic) CCSprite *sprite;
@property (nonatomic) BOOL alive;
@property (nonatomic) float requestedDistanceChange;

- (id) initWithAngle: (float)angle andDistance:(float)distance andCenter:(CGPoint)center andSpriteName:(NSString *)spriteName;
// moves the object by the appropriate distance, according to the elapsed time
// returns the current position of the sprite, after the move is complete.
- (CGPoint) move:(ccTime)delta;
- (CGPoint) position;

@end