//
//  Ship.m
//  Solaris
//
//  Created by Adrian Duyzer on 12-02-19.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.

#import "Ship.h"

@implementation Ship

@synthesize angle = _angle;
@synthesize distance = _distance;
@synthesize sprite = _sprite;
@synthesize alive = _alive;
@synthesize requestedDistanceChange = _requestedDistanceChange;

CGPoint layerCenter;

- (id) initWithAngle:(float)angle andDistance:(float)distance andCenter:(CGPoint)center andSpriteName:(NSString *)spriteName
{
    if ((self = [super init])) {
        self.angle = angle;
        self.distance = distance;
        self.sprite = [CCSprite spriteWithFile:spriteName];
        self.alive = YES;
        layerCenter = center;
    }
    NSLog(@"Ship center is %f x %f", center.x, center.y);
    return self;
}

- (void)setDistance:(float)distance
{
    if (distance < 80)
        distance = 80;
    if (distance > 350)
        distance = 350;
    _distance = distance;
}

- (CGPoint) position
{
    self.sprite.position = ccp(layerCenter.x + self.distance * cos(self.angle), layerCenter.y + self.distance * sin(self.angle));
    return self.sprite.position;    
}

- (CGPoint) move:(ccTime)delta
{
    // When the ship moves, it checks if there is a "requested distance change" quantity that is non-zero.
    // If it is non-zero, it changes the distance by some amount that takes into account the delta of
    // time that has passed.
    // Example: Requested distance change is 305.500000, delta time is 0.016386.
    float distanceToMove = self.requestedDistanceChange * delta + 2;
    if (abs(distanceToMove) > abs(self.requestedDistanceChange)) {
        distanceToMove = self.requestedDistanceChange;
        self.requestedDistanceChange = 0.0;
    } else {
        self.distance += distanceToMove;
        self.requestedDistanceChange -= distanceToMove;        
    }
    
    // I was going to get fancy with the position calculations, but if I want the ship to move faster when closer to the planet,
    // and slower when farther away, I simply need to include the distance in my angle calculation - I can use the distance as a
    // divisor to do this.
    self.angle = self.angle + (300.0 / (self.distance * 2)) * delta;
    return [self position];
}

@end