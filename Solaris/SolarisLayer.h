//
//  HelloWorldLayer.h
//  Solaris
//
//  Created by Adrian Duyzer on 12-01-01.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Ship.h"
#import "Planet.h"

@interface SolarisLayer : CCLayerColor

@property (strong, nonatomic) NSArray *ships;
@property (strong, nonatomic) Planet *planet;

+ (CCScene *)scene;

@end