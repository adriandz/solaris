//
//  HelloWorldLayer.m
//  Solaris
//
//  Created by Adrian Duyzer on 12-01-01.
//  Copyright Adrian Duyzer 2012. All rights reserved.
//
//  Next steps:
//  1. Update the graphics
//  2. Make it so that projectiles that hit the planet stop
//  3. Add some AI!
//  4. Implement the two-tap projectile explosion system
//  5. Implement energy and shields (http://www.raywenderlich.com/4666/how-to-create-a-hud-layer-with-cocos2d)

// Import the interfaces
#import "SolarisLayer.h"
#import "SimpleAudioEngine.h"
#import "InterceptCalculator.h"

@interface SolarisLayer()

@property (strong, nonatomic) NSMutableArray *projectiles;

@end

@implementation SolarisLayer

@synthesize ships = _ships;
@synthesize planet = _planet;
@synthesize projectiles = _projectiles;

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	SolarisLayer *layer = [SolarisLayer node];
	[scene addChild: layer];
	return scene;
}

- (CGPoint) getCenter
{
    CGPoint center;
    center.x = [[CCDirector sharedDirector] winSize].width / 2;
    center.y = [[CCDirector sharedDirector] winSize].height / 2;
    return center;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
    if ( (self=[super init])) {
        CGPoint center = [self getCenter];
        
        // background
        CCSprite *background = [CCSprite spriteWithFile:@"background.png"];
        [background setPosition:center];
        [self addChild:background z:0];
        
        // planet
        self.planet = [[Planet alloc] initWithPosition:center];
        [self addChild:self.planet.sprite];
        
        // ships
        [self initializeShips:center];
        
        self.projectiles = [[NSMutableArray alloc] init];
        
        // deal with pans
        UIPanGestureRecognizer *panRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panning:)] autorelease];
        [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:panRecognizer];
        
        // deal with touches
        UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)] autorelease];
        [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:tapRecognizer];
        
        [self schedule:@selector(nextFrame:)];
        [self schedule:@selector(update:)];
	}
	return self;
}

- (void)initializeShips:(CGPoint)center
{
    // create the first ship, at 200 units away from center
    // we will always assume that the first ship is the "hero" ship, the one that the user controls
    Ship *shipOne = [[Ship alloc] initWithAngle:0.0 andDistance:200.0 andCenter:ccp(center.x, center.y) andSpriteName:@"asteroid.png"];
    shipOne.sprite.position = ccp(center.x + shipOne.distance, center.y + shipOne.distance);
    [self addChild:shipOne.sprite];
    
    // the second ship will also be 200 units away from center, but will be at the opposite angle
    Ship *shipTwo = [[Ship alloc] initWithAngle:180.0 andDistance:300.0 andCenter:ccp(center.x, center.y) andSpriteName:@"asteroid.png"];
    shipTwo.sprite.position = ccp(center.x + shipTwo.distance, center.y + shipTwo.distance);
    [self addChild:shipTwo.sprite];
    
    self.ships = [NSArray arrayWithObjects:shipOne, shipTwo, nil];
}

- (void)nextFrame:(ccTime)delta
{
    for (Ship *ship in self.ships) {
        if (ship.alive)
            [ship move:delta];
    }
}

- (void)panning:(UIPanGestureRecognizer *)recognizer
{    
    if ((recognizer.state == UIGestureRecognizerStateChanged) ||
        (recognizer.state == UIGestureRecognizerStateEnded)) {
        
        CGPoint translation = [recognizer translationInView:[CCDirector sharedDirector].openGLView];
        
        // this is going to let people move it really quickly
        // TODO: fix this, by applying some kind of vector
        Ship *hero = [self.ships objectAtIndex:0];
        
        hero.requestedDistanceChange = hero.requestedDistanceChange - translation.y;
        
        [recognizer setTranslation:CGPointZero inView:[CCDirector sharedDirector].openGLView];
    }    
}

-(void)tap:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"bang.mp3"];
        
        CGPoint location = [recognizer locationInView:[CCDirector sharedDirector].openGLView];
        
        // convert the location
        location = [[CCDirector sharedDirector] convertToGL:location];
        
        // Set up initial location of projectile
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        CCSprite *projectile = [CCSprite spriteWithFile:@"small_dot.png" rect:CGRectMake(0, 0, 20, 20)];        
        Ship *hero = [self.ships objectAtIndex:0];
        projectile.position = [hero position];
        
        [self addChild:projectile];
        [self.projectiles addObject:projectile];
        
        // get the intercept point
        CGPoint intercept = [InterceptCalculator findInterceptFromSource:projectile.position andTouch:location withinBounds:CGRectMake(0, 0, winSize.width, winSize.height)];
        
        NSLog(@"Location.x: %f Location.y: %f Projectile.y: %f Projectile.x: %f intercept X: %f intercept Y: %f", location.x, location.y, projectile.position.x, projectile.position.y, intercept.x, intercept.y);

        // Determine the length of how far we're shooting
        float length = sqrtf(pow((intercept.y - projectile.position.y), 2) + pow((intercept.x - projectile.position.x), 2));
        float velocity = 380/1; // 380pixels/1sec
        float moveDuration = length / velocity;
        
        // Move projectile to actual endpoint
        [projectile runAction:[CCSequence actions:
                               [CCMoveTo actionWithDuration:moveDuration position:intercept],
                               [CCCallFuncN actionWithTarget:self selector:@selector(spriteMoveFinished:)],
                               nil]];
        projectile.tag = 2;
    }
}

-(void)drawDot:(CGPoint)position
{
    CCSprite *dot = [CCSprite spriteWithFile:@"planet.png"];
    dot.position = position;
    [self addChild:dot];
}

-(void)spriteMoveFinished:(id)sender {
    CCSprite *sprite = (CCSprite *)sender;
    if (sprite.tag == 2)
        [self.projectiles removeObject:sprite];
    [self removeChild:sprite cleanup:YES];
}

- (void)update:(ccTime)delta {
    Ship *enemy;
    enemy = [self.ships objectAtIndex:1];
    CGRect enemyRect;
    if (enemy.alive) {
        enemyRect = CGRectMake(enemy.sprite.position.x - (enemy.sprite.contentSize.width/2), 
                               enemy.sprite.position.y - (enemy.sprite.contentSize.height/2), 
                               enemy.sprite.contentSize.width, 
                               enemy.sprite.contentSize.height);
    }
    
    for (CCSprite *projectile in self.projectiles) {
        CGRect projectileRect = CGRectMake(
                                           projectile.position.x - (projectile.contentSize.width/2), 
                                           projectile.position.y - (projectile.contentSize.height/2), 
                                           projectile.contentSize.width, 
                                           projectile.contentSize.height);

        // check for collision with planet, then with enemy
        if (CGRectIntersectsRect(projectileRect, self.planet.rect)) {
            [self removeChild:projectile cleanup:YES];
        } else {
            if (enemy.alive) {
                if (CGRectIntersectsRect(projectileRect, enemyRect)) {
                    /*
                    CCParticleExplosion* explosion = [CCParticleExplosion node];
                    explosion.autoRemoveOnFinish = YES;
                    // TODO: proper texture
                    // explosion.texture = [tempElement texture];
                    explosion.texture = [[CCTextureCache sharedTextureCache] addImage: @"explosion_texture.png"];
                    explosion.startSize = 5.0f;
                    explosion.endSize = 1.0f;
                    explosion.duration = 0.05f;
                    explosion.speed = 30.0f;
                    explosion.anchorPoint = ccp(0.5f,0.5f);
                    // explosion.position = tempElement.position;
                    */
                    CCParticleSun* explosion = [[CCParticleSun alloc]initWithTotalParticles:250];
                    explosion.autoRemoveOnFinish = YES;
                    explosion.startSize = 15.0f;
                    explosion.speed = 80.0f;
                    explosion.anchorPoint = ccp(0.5f,0.5f);
                    explosion.duration = 0.5f;
                    explosion.position = projectile.position;
                    [self addChild: explosion z: self.zOrder+1];
                    
                    enemy.alive = NO;
                    [[SimpleAudioEngine sharedEngine] playEffect:@"explosion.mp3"];
                    [self removeChild:enemy.sprite cleanup:YES];
                    [self removeChild:projectile cleanup:YES];
                }
            }    
        }
    }
}

// on "dealloc" you need to release all your retained objects
- (void)dealloc
{
	// cocos2d will automatically release all the children (Label)
    
	self.ships = nil;
    self.projectiles = nil;
    
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
